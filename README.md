## Modul zur Informierung der Kunden bezüglich Bestandsmengen 
* Kunden können Artikel, die zur Zeit nicht auf Lager sind, auf eine Liste setzen.
* Sobald diese Artikel wieder verfügbar sind, erhalten die Kunden eine Mail