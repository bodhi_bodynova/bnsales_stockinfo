<?php
namespace Bodynova\bnsales_stockinfo\Core;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;

class Events
{
    public function onActivate(){

        $oDb = DatabaseProvider::getDb();
        $create = "CREATE TABLE IF NOT EXISTS `oxarticles2useralert`  (
                      `OXID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL COMMENT 'Record id',
                      `OXARTICLEID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'article id',
                      `OXUSERID` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'User id',
                      `OXLANGID` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Lang Id',
                      `OXTIMESTAMP` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT 'Timestamp',
                      PRIMARY KEY (`OXID`)
                    );";
        try {
            $oDb->execute($create);
        } catch (DatabaseErrorException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        $oMetaData = oxNew(DbMetaDataHandler::class);
        $oMetaData->updateViews();

    }
}