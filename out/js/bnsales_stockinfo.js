function deleteStockInfo(oxid,user){
    $.ajax({
        type: "get",
        url: '/index.php?cl=bnsales_stockinfo&fnc=deleteFromList',
        data: {
            "oxid" : oxid,
            "user" : user
        },
        success: function (response) {
            if(!response){
                console.log('FEHLER');
            } else{
                console.log('Artikel von Liste gelöscht');
                window.location.reload();
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function addToStockList(oxid,user,msg,err){
    $.ajax({
        type: "get",
        url: '/index.php?cl=bnsales_stockinfo&fnc=addToList',
        data: {
            "oxid" : oxid,
            "user" : user
        },
        success: function (response) {
            console.log(response);
            if(response === '2'){
                console.log('Bereits zugefügt');
                alertify.error(err);
            } else {
                console.log('Artikel zur Liste hinzugefügt');
                alertify.success(msg);
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}