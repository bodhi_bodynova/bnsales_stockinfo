<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';


/**
 * Module information
 */
$aModule = [
    'id'           => 'bnsales_stockinfo',
    'title'        => '<img src="../modules/bodynova/bnsales_stockinfo/out/img/favicon.ico" title="Bodynova Haendlershop Stockinfo">odynova Bestandsalarm',
    'description'  => [
        'de' => 'Modul zur Informierung der Kunden über den Artikelbestand',
        'en' => 'Modul zur Informierung der Kunden über den Artikelbestand'
    ],
    'thumbnail'     => 'out/img/logo_bodynova.png',
    'version'      => '1.0.0',
    'author'       => 'Bender&Ewald',
    'email'        => 'c.ewald@bodynova.de',
    'controllers'  => [

        'bnsales_stockinfo' =>
            \Bodynova\bnsales_stockinfo\Application\Controller\bnsales_stockinfo::class,
        'bnsales_stockmail' =>
            \Bodynova\bnsales_stockinfo\Application\Controller\bnsales_stockmail::class,
        'bnsales_stockbunch' =>
            \Bodynova\bnsales_stockinfo\Application\Controller\bnsales_stockbunch::class
    ],
    'extend'       => [

        \OxidEsales\Eshop\Application\Model\User::class =>
            \Bodynova\bnsales_stockinfo\Application\Model\bnsales_stockinfo_user::class
    ],
    'templates' => [
        'stockinfoforuser.tpl' => 'bodynova/bnsales_stockinfo/Application/views/tpl/stockinfoforuser.tpl',
        'stocklist.tpl' => 'bodynova/bnsales_stockinfo/Application/views/tpl/stocklist.tpl',
        'stockmail.tpl' => 'bodynova/bnsales_stockinfo/Application/views/tpl/stockmail.tpl',
    ],
    'settings' => [
        [
            'group' => 'bnsales_stocklevel',
            'name' => 'bnsales_stocklevel',
            'type' => 'str',
        ]

    ],
    'blocks'      => [
        [
            'template' => 'widget/product/stockinfo.tpl',
            'block' => 'bnsales_stockinfo',
            'file' => 'Application/views/blocks/tpl/bnsales_stockinfo.tpl'
        ],
        [
            'template' => 'layout/sidebarLeft.tpl',
            'block' => 'stockinfoList',
            'file' => 'Application/views/blocks/tpl/stockinfoList.tpl'
        ],
        [
            'template' => 'layout/base.tpl',
            'block' => 'base_js',
            'file' => 'Application/views/blocks/tpl/layout/bnsalesstockinfo_base_js.tpl'
        ],
        [
            'template' => 'layout/base.tpl',
            'block' => 'base_style',
            'file' => 'Application/views/blocks/tpl/layout/bnsalesstockinfo_base_css.tpl'
        ]
    ],
    'events' => [
        'onActivate' => '\Bodynova\bnsales_stockinfo\Core\Events::onActivate'
    ]
];

?>
