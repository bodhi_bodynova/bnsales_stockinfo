[{capture append="oxidBlock_content"}]
    [{assign var="oConf" value=$oViewConf->getConfig()}]
    <div class="card" style="margin-top: 180px;  height:auto !important; bottom:180px;" id="newsPanel">
        <div class="card-header">
            <h3 class="card-title"><strong>[{oxmultilang ident="stockinfoalert"}]</strong></h3>
            <div class="pull-right" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "bnsales_stockinfo" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="container-fluid">
                    [{*foreach from=$articlealert item="key"}]
                    [{$key.OXID}]
                    <hr>
                    [{/foreach*}]
                    [{*include file="widget/product/listNeu.tpl" listId="productList" template='stockinfo'*}]
                    [{include file="stocklist.tpl" listId="productList" template='stockinfoList'}]

                </div>
            </div>
        </div>
    </div>
    [{/capture}]

[{*capture append="oxidBlock_sidebar"}]
	[{include file="page/account/inc/account_menu.tpl"}]
[{/capture*}]
[{include file="layout/page.tpl" sidebar="Left" sidebarRight=1 sidebarLeft=1}]


