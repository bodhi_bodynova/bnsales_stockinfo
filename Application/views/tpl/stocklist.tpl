[{if $products|@count gt 0}]

    <div class="table-responsive">
        [{* TODO: mobile Ansicht *}]
        [{* TODO: mobile Ansicht *}]
        [{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'*}]

        [{*/if*}]
        <table id="[{$listId}]" class="table table-condensed" [{if $products|@count eq 1}] style="height:105px;"[{/if}]>
            <thead>
            <tr>
                <th class="d-none d-sm-none d-md-block"></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            [{assign var="currency" value=$oView->getActCurrency()}]
            [{foreach from=$products item=_product name=productlist}]
                [{assign var="_sTestId" value=$listId|cat:"_"|cat:$smarty.foreach.productlist.iteration}]
                [{oxid_include_widget
            cl="oxwArticleBox"
            _parent=$oView->getClassName()
            nocookie=1
            iLinkType=$_product->getLinkType()
            _object=$_product
            anid=$_product->getId()
            sWidgetType=product
            sListType=listitem_lineNeu
            iIndex=$_sTestId
            isVatIncluded=$oView->isVatIncluded()
            showMainLink=$showMainLink
            inlist=$_product->isInList()
            skipESIforUser=1
            }]
                [{/foreach}]
            </tbody>
        </table>
    </div>
    [{/if}]
[{*debug*}]
