[{if $product}]
    <div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]" style="[{if $_product->showCssFlag() == "notOnStock" && $template ne 'stockinfoList'}]top:-3px;[{/if}]"></div>
    [{if $_product->showCssFlag() == "notOnStock" && $template ne 'stockinfoList'}]
        [{if $product->alertInList()}]
            <span class="far fa-bell-slash fa-2x bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="alreadyAdded"}]"></span>
        [{else}]
            <span class="far fa-bell fa-2x bntooltip" style="cursor:pointer;" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="Vormerken"}]" onclick="addToStockList('[{$_product->oxarticles__oxid->value}]','[{$oxcmp_user->oxuser__oxid->value}]','[{oxmultilang ident="addedToList"}]','[{oxmultilang ident="alreadyAdded"}]')"></span>
        [{/if}]
    [{/if}]
[{elseif $_aProduct}]
    [{*$oView->getStatus('$_aProduct->oxarticles__oxid->value','$oxcmp_user->oxuser__oxid->value')*}]
    <div class="stockFlagBox [{if $_aProduct->showCssFlag()}][{$_aProduct->showCssFlag()}][{/if}]" style="[{if $_aProduct->showCssFlag() == "notOnStock" && $template ne 'stockinfoList'}]top:-3px;[{/if}]"></div>
    [{if $_aProduct->showCssFlag() == "notOnStock" && $template ne 'stockinfoList'}]
        [{if $_aProduct->alertInList()}]
            <span class="far fa-bell-slash fa-2x bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="alreadyAdded"}]"></span>
        [{else}]
            <span class="far fa-bell fa-2x bntooltip" style="cursor:pointer;" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="Vormerken"}]" onclick="addToStockList('[{$_aProduct->oxarticles__oxid->value}]','[{$oxcmp_user->oxuser__oxid->value}]','[{oxmultilang ident="addedToList"}]','[{oxmultilang ident="alreadyAdded"}]')"></span>
        [{/if}]
    [{/if}]
[{/if}]
