<?php
$sLangName = "Englisch";

$aLang = array(
    'charset'   => 'UTF-8',
    'stockinfoalert'    => 'Inventory alert',
    'Vormerken'         => 'Click here, to receive an email when this item is in stock again',
    'addedToList'       => 'Item has been added to your list!',
    'alreadyAdded'      => 'Item is already in your list!',
  'infotextListe'     => 'As soon as items on this list are in stock again, you will receive an update by email',
    'anrede'            => 'Hi,<br><br>The following items are now in stock again and can be ordered in the wholesale shop:',
    'schlusstext'       => 'Best regards,<br>Your Bodynova Team',
    'emailBetreff'     => 'Stock alert - Articles now available'

);