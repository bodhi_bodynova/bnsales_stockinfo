<?php
$sLangName = "Deutsch";
$iLangNr = 0;
$aLang = array(
    'charset'           => 'UTF-8',
    'stockinfoalert'    => 'Meine Benachrichtigungen',
    'Vormerken'         => 'Hier klicken, um E-Mail-Benachrichtigung über Verfügbarkeit zu aktivieren',
    'addedToList'       => 'Artikel zur Liste hinzugefügt!',
    'alreadyAdded'      => 'Artikel wurde bereits hinzugefügt!',
    'infotextListe'     => 'Sobald ein Artikel dieser Liste wieder lieferbar ist, werden Sie per Email informiert',
    'anrede'            => 'Sehr geehrter Kunde,<br><br>folgende Artikel, die Sie vorgemerkt haben, sind jetzt wieder verfügbar:',
    'schlusstext'       => 'mit freundlichen Grüßen<br>Ihr Bodynova Team',
    'emailBetreff'     => 'Bestandsalarm -  Artikel wieder lieferbar'
);