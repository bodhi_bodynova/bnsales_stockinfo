<?php
$sLangName = "Französisch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
    'charset' => 'UTF-8',
    'stockinfoalert'    => 'Mes notifications',
    'Vormerken'         => 'Cliquez ici, pour recevoir un e-mail lorsque cet article est à nouveau en stock',
    'addedToList'       => 'L\\\'article a été ajouté à votre liste!',
    'alreadyAdded'      => 'Cet article est déjà dans votre liste!',
  'infotextListe'       =>  'Dès que des articles de cette liste seront à nouveau en stock, vous recevrez une mise à jour par e-mail',
    'anrede'            => 'Bonjour,<br><br>Les articles suivants sont maintenant en stock et peuvent être commandés dans le magasin de gros:',
    'schlusstext'       => 'Cordialement,<br>Votre équipe Bodynova',
    'emailBetreff'     => ' l\'alerte de stock - articles à nouveau disponible'
);