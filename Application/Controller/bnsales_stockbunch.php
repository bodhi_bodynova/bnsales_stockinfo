<?php


namespace Bodynova\bnsales_stockinfo\Application\Controller;

use OxidEsales\Eshop\Application\Model\SmartyRenderer;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\Controller\BaseController;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;

class bnsales_stockbunch extends bnsales_stockmail
{

    protected $_aViewData = null;

    public function __construct($lang = null, $oxuserid = null)
    {
        parent::__construct($lang, $oxuserid);
    }

    public function sendBunch(){

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $query = 'SELECT DISTINCT o2u.OXUSERID,o2u.OXLANGID FROM oxarticles2useralert as o2u LEFT JOIN oxarticles as oa ON o2u.OXARTICLEID = oa.OXID WHERE oa.bnflagbestand = 0 AND oa.OXSTOCK > 9';
        $result = $oDb->getAll($query);

        foreach($result as $key){
            $this->_aViewData['userid'] = $key['OXUSERID'];
            $this->_aViewData['langid'] = $key['OXLANGID'];

            $oUser = new \OxidEsales\EshopCommunity\Application\Model\User();
            $oUser->load($key['OXUSERID']);
            $sEmail = $oUser->oxuser__oxusername->value;
            $sFname = $oUser->oxuser__oxfname->value;
            $sLname = $oUser->oxuser__oxlname->value;

            $email = new \OxidEsales\EshopCommunity\Core\Email();

            Registry::getLang()->setTplLanguage($key['OXLANGID']);
            Registry::getLang()->resetBaseLanguage();
            Registry::getLang()->setBaseLanguage($key['OXLANGID']);

            //$lang = \OxidEsales\Eshop\Core\Registry::getLang();
            //$lang->setTplLanguage($key['OXLANGID']);
            $render = oxNew(SmartyRenderer::class);
            $body = $render->renderTemplate('stockmail.tpl',$this->_aViewData);

            //$body = $render->renderTemplate($this->_sThisTemplate,$this->_aViewData);
            $email->setBody($body);
            $email->setSubject(Registry::getLang()->translateString('emailBetreff'));
            $email->setRecipient($sEmail,$sFname." ".$sLname);
            //$email->setRecipient('svent@live.de');
            $email->setFrom('server@bodynova.de','Bodynova GmbH');
            $email->send();
        }


        $deleteSelectQuery = 'SELECT a.OXARTICLEID FROM oxarticles2useralert AS a LEFT JOIN oxarticles AS b ON a.OXARTICLEID = b.OXID WHERE b.bnflagbestand = 0 AND b.OXSTOCK > 9 GROUP BY a.OXARTICLEID';
        $resultDelete = $oDb->getAll($deleteSelectQuery);
        $arrDelete = array();
        if($resultDelete){

            foreach($resultDelete as $delete){
                array_push($arrDelete,"\"" . $delete['OXARTICLEID'] . "\"");
            }
            $arrDelete = implode(",",$arrDelete);
            try {
                $oDb->execute('DELETE FROM oxarticles2useralert WHERE OXARTICLEID IN (' . $arrDelete . ')');
            } catch (DatabaseErrorException $e) {
                echo $e->getMessage();
            }
        }
    }
}