<?php

namespace Bodynova\bnsales_stockinfo\Application\Controller;


use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;
use OxidEsales\Eshop\Core\UtilsObject;

class bnsales_stockinfo extends \OxidEsales\Eshop\Application\Controller\FrontendController
{
    protected $_sThisTemplate = 'stockinfoforuser.tpl';
    public $order = null;
    public $orderdate = null;
    protected $_aViewData;

    public function render($error = null)
    {
        if($error){
            return;
        }
        parent::render();

        $oUser = $this->getUser();
        //$this->_sUserOxid = $oUser;
        $regobject = Registry::getConfig();

        if ($oUser == null) {
            \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=account&sourcecl=start');
        }
        $oArticleList = oxNew("oxlist");
        $oArticleList->init("oxbase");
        //$oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $oxuserid = $oUser->getId();
        $query = 'SELECT a.OXID FROM oxarticles AS a LEFT JOIN oxarticles2useralert AS b ON a.OXID = b.OXARTICLEID WHERE b.OXUSERID = ?';
        //$result = $oDb->getAll($query,array($oxuserid));
        $oArticleList->selectString($query,array($oxuserid));

        $arrArticles = array();
        if ($oArticleList->count()) {
            foreach ($oArticleList as $oItem) {
                $arrArticles[] = $oItem->__oxid->rawValue;
            }
        }

        $oArtList = oxNew('oxarticlelist');
        $oArtList->loadByArticlesIDs($arrArticles);

        $this->_aViewData['type'] = 'line';
        $this->_aViewData['products'] = $oArtList;

        //$this->_aViewData['articlealert'] = $result;
        return $this->_sThisTemplate;
    }
    public function getBreadCrumb()
    {
        $aPaths = array();
        $aPath = array();
        $oLang = \OxidEsales\Eshop\Core\Registry::getLang();
        $iBaseLanguage = $oLang->getBaseLanguage();

        $aPath['title'] = $oLang->translateString('stockinfoalert', $iBaseLanguage, false);

        $aPath['link'] = $this->getLink();

        $aPaths[] = $aPath;

        return $aPaths;
    }

    /*
    public function getMarkedArticles(){
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $oUser = $this->getUser();
        $oxuserid = $oUser->getId();
        $query = 'SELECT * FROM oxarticles2useralert WHERE OXUSERID = ?';
        $result = $oDb->getAll($query,array($oxuserid));
        $this->_aViewData['articlealert'] = $result;
        return $this->getUser();
    }*/

    public function deleteFromList(){
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $req = new Request();
        $oxid = $req->getRequestParameter('oxid');
        $user = $req->getRequestParameter('user');
        $deleteQuery = 'DELETE FROM oxarticles2useralert WHERE OXARTICLEID = ? AND OXUSERID = ?';

        try {
            $execute = $oDb->execute($deleteQuery, array($oxid, $user));
            print_r($execute);
            die();
        } catch (DatabaseErrorException $e) {
            die(0);
        }
    }

    public function addToList(){
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $req = new Request();
        $oxarticleid = $req->getRequestParameter('oxid');
        $user = $req->getRequestParameter('user');

        // Schon hinzugefügt
        if($this->getStatus($oxarticleid,$user)){
            print_r(2);die();
        }

        $oxid = UtilsObject::getInstance()->generateUId();
        $insertQuery = 'INSERT INTO oxarticles2useralert (OXID,OXARTICLEID,OXUSERID,OXLANGID) VALUES(?,?,?,?)';
        $updateQuery = 'UPDATE oxarticles2useralert SET OXLANGID = ? WHERE OXUSERID = ?';


        try {
            $execute = $oDb->execute($insertQuery, array($oxid,$oxarticleid, $user,Registry::getLang()->getBaseLanguage()));
            $oDb->execute($updateQuery,array(Registry::getLang()->getBaseLanguage(),$user));
            print_r($execute);
            die();
        } catch (DatabaseErrorException $e) {
            die(0);
        }
    }

    public function getStatus($oxarticleid = null, $oxuserid = null){
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $query = 'SELECT * FROM oxarticles2useralert WHERE OXARTICLEID = ? AND OXUSERID = ?';
        $result = $oDb->getAll($query,array($oxarticleid,$oxuserid));
        if($result){
            return 1;
        } else {
            return 0;
        }
    }

    public function sendStockMail(){

    }

}