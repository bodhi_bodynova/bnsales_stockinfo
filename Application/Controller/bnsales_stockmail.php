<?php


namespace Bodynova\bnsales_stockinfo\Application\Controller;


use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Application\Model\SmartyRenderer;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Email;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Language;
use OxidEsales\Eshop\Core\Registry;

class bnsales_stockmail extends \OxidEsales\Eshop\Application\Controller\FrontendController
{
    protected $_sThisTemplate = 'stockmail.tpl';
    public $order = null;
    public $orderdate = null;
    //protected $_aViewData;


    public function __construct($lang = null,$oxuserid = null){

        Registry::getLang()->setBaseLanguage($lang);
        //Registry::getLang()->setBaseLanguage(1);
        //$this->_aViewData['userid'] = $oxuserid;

    }

    public function render(){
        return parent::render();
    }


    public function getDataFromUser(){
        $arrData = array();
        $query = 'select oxart.OXID, o2u.OXARTICLEID, o2u.OXUSERID, o2u.oxlangid
                     from oxarticles2useralert as o2u
                     left join oxarticles as oxart on o2u.OXARTICLEID = oxart.OXID
                     left join oxuser as ou on o2u.OXUSERID = ou.OXID
                     where bnflagbestand = 0 AND o2u.OXUSERID = "beb6b80b4f2714b4c73ae4bd3bdfccfa"';
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $result = $oDb->getAll($query);

        #User
        $oxuserid = $result[0]['OXUSERID'];
        $oUser = new User();
        $oUser->load($oxuserid);

        $arrData['email'] = $oUser->oxuser__oxusername->value;
        $arrData['fname'] = $oUser->oxuser__oxfname->value;
        $arrData['lname'] = $oUser->oxuser__oxlname->value;
        $arrData['articleData'] = array();

        Registry::getLang()->setBaseLanguage($result[0]['oxlangid']);

        foreach($result as $key){

            #Artikel
            $oxartid = $key['OXID'];
            $article = new Article();
            $article->load($oxartid);
            $key['Titel'] = $article->oxarticles__oxtitle->value;

            array_push($arrData['articleData'],$key);

        }
        $this->_aViewData['dataArray'] = $arrData;

    }

    public function getArticlesFromUser($userid,$lang){
        $arrArticles = array();

        $query = 'select oxart.OXID,o2u.OXID as alertId
        from oxarticles as oxart
        left join oxarticles2useralert as o2u on oxart.OXID = o2u.OXARTICLEID
        where bnflagbestand = 0
        and o2u.OXUSERID = ?';
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $result = $oDb->getAll($query, array($userid));
        $i = 0;


        foreach($result as $key){
            $alertId = $key['alertId'];

           $oxartid = $key['OXID'];
           $article = new Article();
           $article->loadInLang($lang,$oxartid);
           $arrArticles[$i]['Titel']= $article->oxarticles__oxtitle->value;
           $arrArticles[$i]['Variante']= $article->oxarticles__oxvarselect->value;
           $arrArticles[$i]['Preis']= $article->oxarticles__oxprice->value;
           $arrArticles[$i]['Artnum']= $article->oxarticles__oxartnum->value;
           $arrArticles[$i]['ID']= $article->oxarticles__oxid->value;

           $i++;

        }

        return $arrArticles;
    }


    public function sendeEmail(){

        parent::render();
        $email = new Email();
        $render = oxNew(SmartyRenderer::class);

        $body = $render->renderTemplate($this->_sThisTemplate,$this->_aViewData);
        $email->setBody($body);

        $email->setSubject('Bodynova Versandbestätigung');
        $email->setRecipient('c.ewald@bodynova.de');
        //$email->setRecipient('svent@live.de');
        $email->setFrom('server@bodynova.de');
        $email->send();

    }

    // TODO : Eventuell wieder auskommentieren
    public function userArray(){
        $arrAsso = array();
        //echo '<pre>';
        $query = 'select oxart.OXID, o2u.OXARTICLEID, o2u.OXUSERID, o2u.oxlangid
                     from oxarticles2useralert as o2u
                     left join oxarticles as oxart on o2u.OXARTICLEID = oxart.OXID
                     left join oxuser as ou on o2u.OXUSERID = ou.OXID
                     where bnflagbestand = 0 ORDER BY o2u.OXUSERID';
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $result = $oDb->getAll($query);

        foreach($result as $key){

            #Artikel
            $oxartid = $key['OXID'];
            $article = new Article();
            $article->loadInLang($key['oxlangid'],$oxartid);
            $key['Titel'] = $article->oxarticles__oxtitle->value;

            #User
            $oxuserid = $key['OXUSERID'];
            $oUser = new User();
            $oUser->load($oxuserid);
            $key['email'] = $oUser->oxuser__oxusername->value;
            $key['fname'] = $oUser->oxuser__oxfname->value;
            $key['lname'] = $oUser->oxuser__oxlname->value;

            $userid = $key['OXUSERID'];
            if(!$arrAsso[$userid]){
                $arrAsso[$userid] = array();
            }
            array_push($arrAsso[$userid],$key);

        }
        $this->_aViewData['userArray'] = $arrAsso;

    }


}